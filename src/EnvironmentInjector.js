 /**
 * This code encapsulates the logic we need that is inspired from
 * react-scripts, Facebook's encapsulation of build tools as part 
 * of their create-react-app utility licensed under MIT.
 * 
 * The goal of this plugin is to inject environment parameters into
 * a client application at build time so that each environment can
 * have its own configurable links with regards to external APIs it is using.
 */

import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';
import dotenvExpand from 'dotenv-expand';

export default class EnvironmentInjector {
    constructor(options = {}) {
        const {
            regex, 
            environment = 'production'
        } = options;

        this.regex = regex;
        this.environment = environment;
    }

    loadDotEnvFiles(env) {
        const dotenvFiles = [
            `${env}.${this.environment}.local`,
            `${env}.${this.environment}`,
            // Don't include `.env.local` for `test` environment
            // since normally you expect tests to produce the same
            // results for everyone
            this.environment !== 'test' && `${env}.local`,
            env,
        ].filter(Boolean);
    
        // Load environment variables from .env* files. Suppress warnings using silent
        // if this file is missing. dotenv will never modify any environment variables
        // that have already been set.  Variable expansion is supported in .env files.
        // https://github.com/motdotla/dotenv
        // https://github.com/motdotla/dotenv-expand
        dotenvFiles.forEach(dotenvFile => {
            if (!fs.existsSync(dotenvFile)) {
                return;
            }
    
            dotenvExpand(
                dotenv.config({
                    path: dotenvFile,
                })
            );
        });
    }

    getFilePath(relativePath) {
        const appDirectory = fs.realpathSync(process.cwd());
        return path.resolve(appDirectory, relativePath);
    }

    getRelevantEnvironmentVariables(envVariables = {}) {
        return Object.keys(envVariables)
            .filter(key => this.regex.test(key))
            .reduce(
                (env, key) => {
                    env[key] = envVariables[key];
                    return env;
                },
                {
                    NODE_ENV: envVariables.NODE_ENV || 'development',
                }
            );
    }

    stringifyEnvironmentVariables(variables = {}) {
        return Object.keys(variables).reduce((env, key) => {
            env[key] = JSON.stringify(variables[key]);
            return env;
        }, {});
    }

    build() {
        this.loadDotEnvFiles(this.getFilePath('.env'));
        let variables = this.getRelevantEnvironmentVariables(process.env);
        return {
            'process.env': this.stringifyEnvironmentVariables(variables),
        };
    }
}


// const webpack = require('webpack');
// const fs = require('fs');
// const path = require('path');
// const dotenv = require('dotenv');
// const dotenvExpand = require('dotenv-expand');

// const ESP_APP = /^ESP_APP_/i;
// //It is critical for build performance that the env is prod
// const NODE_ENV = process.env.NODE_ENV || 'production';

// const loadDotEnvFiles = (env) => {
//     const dotenvFiles = [
//         `${env}.${NODE_ENV}.local`,
//         `${env}.${NODE_ENV}`,
//         // Don't include `.env.local` for `test` environment
//         // since normally you expect tests to produce the same
//         // results for everyone
//         NODE_ENV !== 'test' && `${env}.local`,
//         env,
//     ].filter(Boolean);

//     // Load environment variables from .env* files. Suppress warnings using silent
//     // if this file is missing. dotenv will never modify any environment variables
//     // that have already been set.  Variable expansion is supported in .env files.
//     // https://github.com/motdotla/dotenv
//     // https://github.com/motdotla/dotenv-expand
//     dotenvFiles.forEach(dotenvFile => {
//         if (!fs.existsSync(dotenvFile)) {
//             return;
//         }

//         dotenvExpand(
//             dotenv.config({
//                 path: dotenvFile,
//             })
//         );
//     });
// }

// const getFilePath = relativePath => {
//     const appDirectory = fs.realpathSync(process.cwd());
//     return path.resolve(appDirectory, relativePath);
// }

// const getRelevantEnvironmentVariables = (envVariables = {}) => {
//     return Object.keys(envVariables)
//         .filter(key => ESP_APP.test(key))
//         .reduce(
//             (env, key) => {
//                 env[key] = envVariables[key];
//                 return env;
//             },
//             {
//                 NODE_ENV: envVariables.NODE_ENV || 'development',
//             }
//         );
// }

// const stringifyEnvironmentVariables = (variables = {}) => {
//     return Object.keys(variables).reduce((env, key) => {
//         env[key] = JSON.stringify(variables[key]);
//         return env;
//     }, {})
// }

// const buildInjectEnvironmentPlugin = () => {
//     loadDotEnvFiles(getFilePath('.env'));
//     let variables = getRelevantEnvironmentVariables(process.env);
//     return {
//         'process.env': stringifyEnvironmentVariables(variables),
//     };
// }

// module.exports = new webpack.DefinePlugin(buildInjectEnvironmentPlugin());
