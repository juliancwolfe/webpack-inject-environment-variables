import EnvironmentInjector from './EnvironmentInjector';
import webpack from 'webpack';

export default class InjectEnvironmentPlugin {
    constructor(options) {
        const environmentInjector = new EnvironmentInjector(options);
        return new webpack.DefinePlugin(environmentInjector.build());
    }
}